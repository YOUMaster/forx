$(document).ready(function() {

	//phones dropdown
	$(document).on('click', '.header__phones', function(event) {
		event.preventDefault();
		$(this).toggleClass('header__phones--active');
	});

	//languages dropdown
	$(document).on('click', '.header__languages', function(event) {
		event.preventDefault();
		$(this).toggleClass('header__languages--active');
	});

	//scroll to categories
	$(document).on("click",".slider__down", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top - 67;
		$('body,html').animate({scrollTop: top}, 1000);
	});	

	//owl init
	$('#owl-carousel-1').owlCarousel({
		loop: true,
		items: 1,
		autoplay: true,
		autoplayTimeout: 3000
	})

	//owl init for news
	$('#owl-carousel-2').owlCarousel({
		loop: true,
		items: 2,
		//autoplay: true,
		nav: true,
		navText: ['<','>'],
		dots: false
	})

	$(document).find(".owl-dots").wrap("<div class='container'></div>");

	//wow init
	new WOW().init();

	//Replace category section on gadgets
	function moveElements(){
		var width = document.documentElement.clientWidth;
		var height = document.documentElement.clientHeight;
		if (width <= 982) {
			$('#categories').after($('.home__news'));
		}
		else{
			$('#categories').after($('.home__catalog'));
		}
	}
	moveElements();
	$(window).resize(moveElements);

});